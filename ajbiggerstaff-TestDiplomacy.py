

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    def test_solve_1(self):
        r = StringIO("""A Austin Hold
                        B Dallas Move Austin
                        C Houston Support A
                        D DC Support A
                        E Seattle Support A
                        F Vancouver Support B
                        G NewYork Support B
                        H Boston Support B""")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), """A [dead]\nB [dead]\nC Houston\nD DC\nE Seattle\nF Vancouver\nG NewYork\nH Boston\n""")

    def test_solve_2(self):
        r = StringIO("""A Dallas Hold
                        B Austin Move Dallas
                        C Austin Move Dallas
                        D Austin Move Dallas
                        E Austin Move Dallas
                        F Austin Move Dallas""")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), """A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n""")
    
    def test_solve_3(self):
        r = StringIO("""A Dallas Support B
                        B Barcelona Support C
                        C London Support D
                        D Paris Move Dallas
                        E Austin Move Barcelona
                        F Berlin Move London""")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), """A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n""")

# ----
# main
# ----

if __name__ == "__main__":
    main()
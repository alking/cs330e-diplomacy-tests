from unittest import main, TestCase
from io import StringIO
from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_read, diplomacy_solve

class TestDiplomacy(TestCase):
    # ---read---
    def test_read_1(self):
        s = StringIO("A Berlin Support B\nB Madrid Hold\nC Detroit Move London\nD London Move Madrid")
        l = diplomacy_read(s)
        self.assertEqual(l, [("A", "Berlin", "Support", "B"), ("B", "Madrid", "Hold"), ("C", "Detroit", "Move", "London"), ("D", "London", "Move", "Madrid")])

    def test_read_2(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        l = diplomacy_read(s)
        self.assertEqual(l, [("A", "Madrid", "Hold"), ("B", "Barcelona", "Move", "Madrid"), ("C", "London", "Support", "B")])

    def test_read_3(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        l = diplomacy_read(s)
        self.assertEqual(l, [('A', 'Madrid', 'Hold'), ('B', 'Barcelona', 'Move', 'Madrid')])

    # ---print---

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {"A": "[dead]", "B": "[dead]", "C": "[dead]", "D": "[dead]"})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {"A": "Madrid", "B": "[dead]", "C": "London", "D": "NewYork"})
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London\nD NewYork\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {"A": "Madrid", "B": "Tokyo", "C":"London"})
        self.assertEqual(w.getvalue(), "A Madrid\nB Tokyo\nC London\n")

    # ---eval---

    def test_eval_1(self):
        v = diplomacy_eval([("A", "Madrid", "Hold"), ("B", "Barcelona", "Move", "Madrid"), ("C", "London", "Support", "B")])
        self.assertEqual(v, {"A": "[dead]", "B": "Madrid", "C": "London"}) 

    def test_eval_2(self):
        v = diplomacy_eval([('A', 'Madrid', 'Hold'), ('B', 'Barcelona', 'Move', 'Madrid')])
        self.assertEqual(v, {"A": "[dead]", "B": "[dead]"})

    def test_eval_3(self):
        v = diplomacy_eval([('A', 'Madrid', 'Hold'), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Support', 'B'), ('D', 'Austin', 'Move', 'London')])
        self.assertEqual(v, {"A": "[dead]", "B": "[dead]", "C": "[dead]", "D": "[dead]"})
        

    def test_eval_4(self):
        v = diplomacy_eval([('A', 'Madrid', 'Hold'), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Move', 'Madrid')])
        self.assertEqual(v, {"A": "[dead]", "B":"[dead]", "C": "[dead]"})

    def test_eval_5(self):
        v = diplomacy_eval([('A', 'Madrid', 'Hold'), ('B', 'Barcelona', 'Hold'), ('C', 'London', 'Hold')])
        self.assertEqual(v, {"A": "Madrid", "B": "Barcelona", "C": "London"})

    # ---solve---

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")


    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n C London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Paris Move Perdu")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Perdu\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

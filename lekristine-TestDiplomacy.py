#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# Diplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = StringIO("A Madrid Hold\n")
        i = diplomacy_read(s)
        self.assertEqual(i, [["A", "Madrid", "Hold"]])

    def test_read_2(self):
        s = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        i = diplomacy_read(s)
        self.assertEqual(i, [["A", "Madrid", "Hold"],
                             ["B", "Barcelona", "Move", "Madrid"],
                             ["C", "London", "Support", "B"],
                             ["D", "Austin", "Move", "London"]])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        actions = [["A", "Madrid", "Hold"]]
        results = diplomacy_eval(actions)
        expectedResults = {"A":"Madrid"}
        self.assertEqual(results, expectedResults)

    def test_eval_2(self):
        actions = [["A", "Madrid", "Hold"],
                    ["B", "Barcelona", "Move", "Madrid"],
                    ["C", "London", "Support", "B"],
                    ["D", "Austin", "Move", "London"]]
        results = diplomacy_eval(actions)
        expectedResults = {"A": "[dead]","B":"[dead]","C":"[dead]","D":"[dead]"}
        self.assertEqual(results, expectedResults)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        results = {"A":"Madrid"}
        diplomacy_print(w, results)
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_print_2(self):
        w = StringIO()
        results = {"A":"[dead]","B":"Madrid","C":"London"}
        diplomacy_print(w, results)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n"
        )

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n"
        )

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n"
        )

    def test_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        )

    def test_solve_5(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n"
        )

    def test_solve_6(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n"
        )

    def test_solve_7(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        )


# ----
# main
# ----
if __name__ == "__main__":
    main()

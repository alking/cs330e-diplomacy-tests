from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):

	def test_solve(self):
   		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
   		w = StringIO()
   		diplomacy_solve(r, w)
   		v = w.getvalue()
   		self.assertEqual(v, "A [dead]\nB Madrid\nC London\n")

	def test_solve1(self):
   		a = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
   		b = StringIO()
   		diplomacy_solve(a, b)
   		c = b.getvalue()
   		self.assertEqual(c, ("A [dead]\nB [dead]\nC [dead]\n"))

	def test_solve2(self):
   		x = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
   		y = StringIO()
   		diplomacy_solve(x, y)
   		z = y.getvalue()
   		self.assertEqual(z, "A [dead]\nB [dead]\n")


	


	'''#eval
				def test_eval_1(self):
					v= diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid"])
					self.assertEqual(v, [['A [dead]'], ['B [dead]'], ['C [dead]']])
			
				def test_eval_2(self):
					v= diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"])
					self.assertEqual(v, [['[A [dead]'], ['B Madrid'], ['C London']])
			
				def test_eval_3(self):
					v= diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid"])
					self.assertEqual(v, [['A [dead]'], ['B [dead]']])
			
				def test_eval_4(self):
					v= diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"])
					self.assertEqual(v, [['A [dead]'], ['B [dead]'], ['C [dead]'], ['D [dead]']])
			
				def test_eval_5(self):
					v= diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B"])
					self.assertEqual(v, [['A [dead]'], ['B Madrid'], ['C [dead]'], ['D Paris']])'''

	

	#print


'''	def test_print_1(self):
		w= StringIO()
		diplomacy_print(w, ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"], ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])
		self.assertEqual(w.getvalue(), ("A [dead] \n", "B [dead] \n", "C [dead] \n", "D [dead] \n"))


	def test_print_2(self):
		w= StringIO()
		diplomacy_print(w, ["A Madrid Hold", "B Barcelona Move Madrid"],["A [dead]", "B [dead]"])
		self.assertEqual(w.getvalue(), ("A [dead] \n", "B [dead] \n"))

	def test_print_3(self):
		w = StringIO()
		diplomacy_print(w, ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B"], ["A [dead]", "B Madrid", "C [dead]", "D Paris"])
		self.assertEqual(w.getvalue(), ("A [dead] \n", "B Madrid \n", "C [dead] \n", "D Paris \n"))

	def test_print_4(self):
		w = StringIO()
		diplomacy_print(w, ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid"], ["A [dead]", "B [dead]", "C [dead]"])
		self.assertEqual(w.getvalue(), ("A [dead] \n", "B [dead] \n", "C [dead] \n"))

	def test_print_5(self):
		w = StringIO()
		diplomacy_print(w, ["A Madrid Hold", "B Barcelona Move Madrid", ["C London Support B"], ["A [dead]", "B Madrid", "C London"]])
		self.assertEqual(w.getvalue(), ("A [dead] \n", "B Madrid \n", "C London \n"))

	'''
	
if __name__ == "__main__":
	main()
